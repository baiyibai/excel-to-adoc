# 2024 白一百

# This script converts an Excel file with merged tables to AsciiDoctor format
# It also needs to iterate through sheets

# Process
# 1) Prompt the user for the location of the file (pathcopy) r"". https://pathcopycopy.github.io/
# 2) Get the current date and time for for ISO8086/RFC3339 format https://ijmacd.github.io/rfc3339-iso8601/.
# 3) Open the Excel file, iterate through all the sheets to generate the output asciidoctor file names.
# 4) Create the AsciiDoctor-formatted tables line-by-line while keeping track of row and column spans.

import os
from openpyxl import load_workbook
from pathlib import Path
import datetime

def open_excel_file(excel_path, just_input_file_name):
    # Load Excel workbook
    wb = load_workbook(filename=excel_path)

    for sheet_name in wb.sheetnames:
        sheet = wb[sheet_name]
        
        # sheet = wb.active
        num_columns = sheet.max_column
        output_file = f"{the_time}_{just_input_file_name}_{sheet.title}.adoc"
        write_adoc(sheet, output_file, num_columns)
        print(f"Saved AsciiDoctor output of {just_input_file_name} - {sheet.title} to {output_file}.")
    # Close the workbook; this doesn't solve the permission issue
    wb.close()

def write_adoc(sheet, output_file, num_columns):
    # Write AsciiDoc output
    with open(output_file, 'w', encoding='utf-8') as f:
        # Write the number of columns
        f.write(f'[cols={num_columns}]\n')
        # Write table header
        f.write('|===\n')

        # Iterate through each row in the worksheet
        for row in sheet.iter_rows():
            f.write('\n')

            # Hallucination, this section cuts off content
            prev_cell_merged = False
            prev_colspan = 1
            
            # Iterate through each cell in the row
            for cell in row:
                if cell.value is None:
                    continue  # Skip empty cells; both merged and intentionally blank!

                # Handle basic AsciiDoctor formatting
                if cell.font.bold == True and cell.font.italic == True:
                    cell.value = f"*_{cell.value}_*"
                if cell.font.bold == True and cell.font.italic != True:
                    cell.value = f"*{cell.value}*"
                if cell.font.bold != True and cell.font.italic == True:
                    cell.value = f"_{cell.value}_"

                # Handle line breaks
                cell.value = cell.value.replace('\n', ' +\n')
                rowspan, colspan = get_span(sheet, cell)

                # Hallucination, this section cuts off content
                # if prev_cell_merged and prev_colspan > 1:
                #     # Adjust rowspan if the previous cell was merged and had colspan
                #     rowspan -= 1
                
                # Generate the AsciiDoctor row and column spans
                rowspan_to_print = None
                period_char = None
                colspan_to_print = None
                plus_char = None
                only_rowspan_period_char = None
                if rowspan > 1:
                    rowspan_to_print = str(rowspan)
                    plus_char = "+"
                    if colspan == 1: # Only column span
                        only_rowspan_period_char = "."
                if colspan > 1:
                    period_char = "."
                    colspan_to_print = str(colspan)
                    plus_char = "+"

                # Write the row and column spans if they exist
                f.write(f"{only_rowspan_period_char}" if only_rowspan_period_char is not None else "")
                f.write(f"{colspan_to_print}" if colspan_to_print is not None else "")
                f.write(f"{period_char}" if period_char is not None else "")
                f.write(f"{rowspan_to_print}" if rowspan_to_print is not None else "")
                f.write(f"{plus_char}" if plus_char is not None else "")

                # Write the cell content
                f.write(f"|{cell.value}\n")
                prev_cell_merged = rowspan > 1 or colspan > 1
                prev_colspan = colspan

        # Write table footer
        f.write('\n')
        f.write('|===\n')
        f.close()

def get_span(sheet, cell):
    rowspan = 1
    colspan = 1
    for merged_range in sheet.merged_cells.ranges:
        if cell.coordinate in merged_range:
            rowspan = merged_range.max_row - merged_range.min_row + 1
            colspan = merged_range.max_col - merged_range.min_col + 1
            break
    return rowspan, colspan

print("excel-to-adoc")
print("This script converts an Excel file to AsciiDoc format.\r\n")
print("Note: Empty cells are skipped!\r\n")
# print("Make sure the target .xlsx file is not open in Excel!\r\n")

while True:
    excel_path = input("Input the file path of the Excel file to convert; use PathCopy to make it easier, or press control+c to exit.:\r\n")
    if excel_path != None:

        # Check if file is open # https://stackoverflow.com/questions/6825994/check-if-a-file-is-open-in-python
        just_input_file_name = Path(excel_path).name
        just_input_file_name_test_open = "open_"+just_input_file_name 
        try:
            os.rename(Path(excel_path), Path(str(Path(excel_path).parents[0])+just_input_file_name_test_open))
            os.rename(Path(str(Path(excel_path).parents[0])+just_input_file_name_test_open), Path(excel_path))
        except PermissionError:
            print(f'\nERROR: The {just_input_file_name} is still open in Excel. Close the file before continuing just in case.\n')
            continue

        the_time = datetime.datetime.now().strftime("%Y-%m-%d_%H%M%S")
        open_excel_file(excel_path, just_input_file_name)
        print(f"Conversion finished.\n")
